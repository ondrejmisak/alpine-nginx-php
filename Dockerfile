# Powerfull and extra small docker image for your PHP apps.
# https://gitlab.com/ondrejmisak/alpine-nginx-php

FROM alpine:3.8

MAINTAINER Ondřej Misák <email@ondrejmisak.cz>

# Environment variables
## PHP
### Setting server timezone.
ENV TIME_ZONE='Europe/Prague'
## NGINX
### Setting server index file.
ENV INDEX_FILE='index.php'
### Set document root of nginx site.
ENV DOCUMENT_ROOT='/var/www/html/www'

# Argument variables
## User and group name (if change, you must edit manifest/supervisor/supervisor.conf and manifest/php/php-fpm.conf too)
ARG USER='docker-user'
ARG GROUP='docker-apps'
## Version of PHP which will be installed.
ARG PHP_VER='7.2.8'
## Php core module names without prefix "php7-". Iconv extension is included automatically.
ARG PHP_CORE_PACKAGES='fpm opcache session intl mbstring json fileinfo tokenizer memcached curl gd pdo_sqlite pdo_mysql mysqli'
## Php other module names without prefix "php7-".
ARG PHP_OTHER_PACKAGES='xml simplexml xmlwriter dom bcmath ctype calendar zip ssh2'
## Packages needed for build iconv extension from source.
ARG BUILD_PACKAGES='wget build-base php7-dev'
## Packages needed for correctly working flow of this image.
ARG ESSENTIAL_PACKAGES='sed nginx supervisor nano'

RUN echo '@testing https://dl-4.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories && \
    apk update && \
    apk upgrade --no-cache && \
    # Setting the timezone.
    apk add --no-cache tzdata && \
    cp /usr/share/zoneinfo/$TIME_ZONE /etc/localtime && \
    # # Setting the timezone.
    # Create user and group.
    addgroup -S $GROUP && \
    adduser -H -D -S -G $GROUP $USER && \
    # # Create user and group.
    # Install utility, essential packages and PHP modules.
    apk add --no-cache $ESSENTIAL_PACKAGES php7 $(printf 'php7-%s\n' $PHP_CORE_PACKAGES $PHP_OTHER_PACKAGES) && \
    # # Install utility and essential packages.
    # File system actions.
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log && \
    # # File system actions.
    # Fix iconv extension.
    apk add --no-cache --virtual .php-build-dependencies $BUILD_PACKAGES && \
    apk add --no-cache gnu-libiconv-dev@testing && \
    (mv /usr/bin/gnu-iconv /usr/bin/iconv; mv /usr/include/gnu-libiconv/*.h /usr/include; rm -rf /usr/include/gnu-libiconv) && \
    mkdir -p /opt && \
    cd /opt && \
    wget -q https://secure.php.net/distributions/php-$PHP_VER.tar.gz && \
    tar xzf php-$PHP_VER.tar.gz && \
    cd php-$PHP_VER/ext/iconv && \
    phpize && \
    ./configure --with-iconv=/usr && \
    make && \
    make install && \
    mkdir -p /etc/php7/conf.d && \
    echo 'extension=iconv.so' >> /etc/php7/conf.d/iconv.ini && \
    # # Fix iconv extension.
    # Cleanup.
    apk del tzdata .php-build-dependencies && \
    rm -rf /var/cache/apk/* /tmp/* /opt/*
    # # Cleanup.

# Add config files.
COPY ./manifest/php/ /etc/php7/
COPY ./manifest/nginx/ /etc/nginx/
COPY ./manifest/supervisor/supervisord.conf /etc/
COPY ./manifest/supervisor/services /etc/supervisord.d/

# Copy init script.
COPY ./manifest/entrypoint.sh /
RUN chmod +x /entrypoint.sh

# Set app work directory.
WORKDIR /var/www/html

# Expose Ports.
EXPOSE 443 80

# Run init script and supervisord.
ENTRYPOINT ["/entrypoint.sh"]
